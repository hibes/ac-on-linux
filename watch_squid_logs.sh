#!/usr/bin/env bash

tail -f \
  squid/log/cache.log \
  squid/log/access.log
