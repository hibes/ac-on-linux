#!/usr/bin/env bash

as_user() {
  export HOME_BKP="${HOME}"
  export HOME="/home/${USER}"
  su --preserve-environment -l "${USER}" -c "$@"
  export HOME="${HOME_BKP}"
}

set -x

#ip route del default
#ip route add default via 172.25.25.254

#ip route show

#sleep 9999
#exit -1

until [ -f '/usr/local/share/ca-certificates/cert.crt' ]; do
  sleep 1
done

update-ca-certificates

if [ $# -ne 0 ]; then
  CMD=

  for i in "$@"; do
    CMD="${CMD} ${i}"
  done
fi

CUSTOM_POL_SHORTCUTS="${GAMER_HOME}/custom-pol-shortcuts"
MY_DOCUMENTS_AC="${GAMER_HOME}/Asheron's Call"
POL_SHORTCUTS_FLDR="${GAMER_HOME}/.PlayOnLinux/shortcuts"
USER_PREFERENCES_INI="${MY_DOCUMENTS_AC}/UserPreferences.ini"

FILES=()

if [ ! -f "${USER_PREFERENCES_INI}" ]; then
  mkdir -p "${MY_DOCUMENTS_AC}"

  cp "${GAMER_HOME}/UserPreferences.example.ini" "${USER_PREFERENCES_INI}"

  FILES=( "${FILES[@]}" "${USER_PREFERENCES_INI}" )
fi

for i in "${CUSTOM_POL_SHORTCUTS}/"*; do
  BN="$(basename "${i}")"
  COPY_OF_SHORTCUT="${POL_SHORTCUTS_FLDR}/${BN}"

  cp -r "${i}" "${COPY_OF_SHORTCUT}"

  FILES=( "${FILES[@]}" "${COPY_OF_SHORTCUT}" )
done

chown "${USER}" "${FILES[@]}"

as_user "${CMD:-playonlinux}"
