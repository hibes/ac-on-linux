#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )"
THIS_DIR="${THIS_DIR:?}"

. "${THIS_DIR}/lib.sh"

SQUID="$(find_container_name squid)"

docker-compose exec squid /bin/bash -c "echo -ne '' | tee /var/log/squid/access.log"
docker kill -s HUP "${SQUID}"
