#!/usr/bin/env bash

set -e

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )"
THIS_DIR="${THIS_DIR:?}"
ROOT_DIR="$(realpath "${THIS_DIR}/..")"
CLIENT_DIR="${THIS_DIR}/client"

PRIV="${THIS_DIR}/cert.pem"
#CRT="${THIS_DIR}/cert.crt"
CLIENT="${CLIENT_DIR}/cert.crt"
CONF="${THIS_DIR}/conf"
SIZE=2048

FILES=( "${PRIV}" "${CLIENT}" )

openssl req \
  -config "${CONF}" \
  -new \
  -newkey rsa:${SIZE} \
  -sha256 \
  -days 36500 \
  -nodes \
  -x509 \
  -extensions v3_ca \
  -keyout "${PRIV}" \
  -out "${PRIV}"

chmod 700 "${PRIV}"

openssl x509 -in "${PRIV}" -inform PEM -out "${CLIENT}"
#cp "${CRT}" "${CLIENT}"

for i in "${FILES[@]}"; do
  chown root "${i}"
done
