#!/usr/bin/env bash

set -e

AC_ON_LINUX_LIB_SH_DEFINED_VARS=( "AC_ON_LINUX_LIB_SH_DEFINED_VARS" "AC_ON_LINUX_LIB_SH_CONTAINER_NAME" "AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME" )

find_container_name() {
  docker-compose ps "${1}" | tail -n 1 | awk '{print $1}'
}

find_dc_container_name() {
  docker-compose ps --services | grep "${1}" | tail -n 1
}

set_container_name() {
  AC_ON_LINUX_LIB_SH_CONTAINER_NAME="$(find_container_name "$1")"
  AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME="$(find_dc_container_name "$1")"
}

dcbuild() {
  docker-compose build ${AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME}
}

dcstop() {
  docker-compose stop ${AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME}
}

dcrm() {
  docker-compose rm -f ${AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME}
}

dcup() {
  docker-compose up -d ${AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME}
}

dclogs() {
  docker-compose logs -f ${AC_ON_LINUX_LIB_SH_DC_CONTAINER_NAME}
}

dcrestart() {
  dcstop && dcrm && dcup
}

build_and_restart() {
  if [ $# -gt 0 ]; then
    set_container_name "$1"
  fi

  dcbuild && dcrestart
}

undef_all_ac_on_linux_lib_sh() {
  local X="${BASH_SOURCE[0]}"
  local PATTERN='\(\) {$'
  local i=

  for i in $(cat "${X}" | grep -E "${PATTERN}" | sed -E "s/.{4}$//"); do
    unset -f $i
  done

  for i in ${AC_ON_LINUX_LIB_SH_DEFINED_VARS[@]}; do
    unset -v $i
  done
}
